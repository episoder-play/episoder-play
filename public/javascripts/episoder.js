function readyFunction()
{
    $(".accordion").accordion();
    $(".accordion").find("ul").find("li:first").addClass("first");
    $(".bubbleHover").hover(
        function()
        {
            $(this).find(".bubbleHoverTarget").removeClass("hidden");
        },
        function()
        {

            $(this).find(".bubbleHoverTarget").addClass("hidden");
        }
    );
    $(".switcher img.in").click(
           function(event)
           {
                $(this).css({'opacity' : '0.4','cursor':'auto' });
                $(".switcher img.out").css({'cursor':'pointer','opacity' : '1.0' });
                $("#colTwo > div.eplist").removeClass("asline");
           }).css({'opacity' : '0.4' });
    $(".switcher img.out").click(
           function(event)
           {
                $(this).css({'opacity' : '0.4', 'cursor':'auto' });
                $(".switcher img.in").css({'cursor':'pointer','opacity' : '1.0' });
                $("#colTwo > div.eplist").addClass("asline");
           }).css({'cursor':'pointer'});


}

function consumeAction(clickedDiv, hasRedClass)
{
    if(hasRedClass)
    {
        clickedDiv.closest(".bubbleHover").find(".notconsumed")
            .removeClass('notconsumed')
            .addClass('consumed');
    }
    else
    {
        clickedDiv.closest(".bubbleHover").find(".consumed")
            .removeClass('consumed')
            .addClass('notconsumed');
    }

}
