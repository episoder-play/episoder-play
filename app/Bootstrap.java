import models.Media;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

/**
 * TODO: Doku
 *
 * @author knm
 */
@OnApplicationStart
public class Bootstrap extends Job
{
    public void doJob()
    {
        if(Media.count() == 0)
        {
            System.out.println("inserting test data: vampire diaries");
            Fixtures.loadModels("vampirediaries.yml");
        }
    }
}
