package models;

import play.data.validation.Email;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * User model
 *
 * @author knm
 */
@Entity
public class User extends Model
{
    @Email
    @Required
    public String email;

    @Required
    public String password;

    public String fullName;
    public boolean isAdmin;

//    @ManyToMany(cascade= CascadeType.PERSIST)
//    List<Media> medias;

    @OneToMany(mappedBy = "user",cascade= CascadeType.ALL)
    public List<Media> wishlist;

    public User(String email, String password, String fullName)
    {
        this.email = email;
        this.password = password;
        this.fullName = fullName;
//        medias = new ArrayList<Media>();
        wishlist = new ArrayList<Media>();
    }

    public static User connect(String email, String password)
    {
        return find("byEmailAndPassword", email, password).first();
    }

    public String toString()
    {
        return email;
    }

}
