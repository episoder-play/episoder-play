package models;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Base class for medias
 *
 * @author knm
 */
@Entity
public class Media extends Model
{
    public Date publishingDate;
    public boolean consumed;
    public String author;

    public Blob picture;

    @Required
    public String title;
    public String subtitle;
    public String number;
    public String identifier;

    @ManyToOne
    public User user;

    @MaxSize(10000)
    public String description;

    @ManyToMany(cascade= CascadeType.PERSIST)
    public Set<Tag> tags;

    @OneToMany(cascade= CascadeType.PERSIST)
    public List<Media> children;

    @Enumerated(EnumType.STRING)
    public MediaType type;

    public Media(Date publishingDate, String author, String title,
            String subtitle, String number, String identifier,
            String description)
    {
        this.publishingDate = publishingDate;
        this.author = author;
        this.title = title;
        this.subtitle = subtitle;
        this.number = number;
        this.identifier = identifier;
        this.description = description;
        this.consumed = false;
        this.tags = new TreeSet<Tag>();
        this.children = new ArrayList<Media>();
    }

    public Media tagItWith(String name)
    {
        tags.add(Tag.findOrCreateByName(name));
        return this;
    }


    public Media tagItWith(String name, boolean deep)
    {
        tags.add(Tag.findOrCreateByName(name));
        for(Media child : children)
        {
            child.tagItWith(name, true);
        }
        return this;
    }

    public Media add(Media... children)
    {
        this.children.addAll(Arrays.asList(children));
        return this;
    }

    public static List<Media> upcoming()
    {
        Date today = new Date();
        List<Media> result;
        result = Media.find("select m from Media m where publishingDate > :today order by publishingDate asc")
                .bind("today", today).fetch(10);
        return result;
    }

    public static List<Media> past(int size)
    {
        Date today = new Date();
        List<Media> result;
        result = Media.find("select m from Media m where publishingDate < :today order by publishingDate desc")
                .bind("today", today).fetch(size);
        return result;
    }

    public static List<Media> consumed()
    {
        return Media.find("byConsumed", true).fetch();
    }

    public boolean isUpcoming()
    {
        return this.publishingDate.compareTo(new Date()) > 0;
    }

    public static boolean isShowConsumed(String title)
    {
        boolean result = true;
        List<Media> shows = find("byTitle", title).fetch();
        for(Media show : shows)
        {
            if(!show.consumed)
            {
                result = false;
                break;
            }
        }

        return result;
    }


    @Override
    public String toString()
    {
        return String.format("%s > %s (%s)", title, subtitle, identifier);
    }
}
