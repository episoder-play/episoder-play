package models;

import play.data.validation.Required;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * TODO: Doku
 *
 * @author knm
 */
@Entity
public class TVShow extends Model
{
    // should correspond to media.title
    @Required
    public String title;
    public Blob picture;

    @ManyToMany(cascade= CascadeType.PERSIST)
    public Set<Tag> tags = new TreeSet<Tag>();


    @Override
    public String toString()
    {
        return title;
    }

    public Tag tagItWith(String name)
    {
        return Tag.findOrCreateByName(name);
    }


    private static List<Object> tagsFromTitle(String title)
    {
        TVShow show = find("Title", title).first();
        return  Arrays.asList(show.tags.toArray());
    }
}
