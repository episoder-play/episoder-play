package models;

/**
 * Types of media
 *
 * @author knm
 */
public enum MediaType
{
    TV_SHOW, EPISODE, BOOK, MOVIE, AUDO
}
