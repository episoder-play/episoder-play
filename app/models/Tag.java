package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import java.util.List;
import java.util.Map;

/**
 * TODO: Doku
 *
 * @author knm
 */
@Entity
public class Tag extends Model
{
    public String name;

    private Tag(String name)
    {
        this.name = name;
    }


    public static List<Map> getCloud()
    {
        List<Map> result = Tag.find(
                "select new map(t.name as tag, count(p.id) as pound) from Post p join p.tags as t group by t.name order by t.name"
        ).fetch();
        return result;
    }

    public static Tag findOrCreateByName(String name)
    {
        Tag tag = Tag.find("byName", name).first();
        if(tag == null)
        {
            tag = new Tag(name);
        }
        return tag;
    }
}
