package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.joda.time.Minutes;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import models.Media;
import models.MediaType;
import models.TVShow;
import models.User;
import play.data.binding.Binder;
import play.data.validation.Error;
import play.db.Model;
import play.db.jpa.Blob;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;
import play.libs.MimeTypes;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import play.test.Fixtures;

@With(Secure.class)
public class Application extends Controller
{

    @Before
    static void setConnectedUser() throws Throwable
    {
        if(Security.isConnected())
        {
            User user = User.find("byEmail", Security.connected()).first();
            if(user != null)
            {

                List<Media> episodes = Media.past(10);
                renderArgs.put("episodes", episodes);

                List<Media> upcoming = Media.upcoming();
                renderArgs.put("upcoming", upcoming);

                List<Media> past = Media.past(4);
                renderArgs.put("past", past);

                List<String> tvshows = getTvShows();
                renderArgs.put("tvshows", tvshows);

                // fix of org.hibernate.LazyInitializationException
                renderArgs.put("wishlist", user.wishlist);
                renderArgs.put("user", user);
            }
            else
            {
                Secure.logout();
            }

        }
    }

    public static void index()
    {
        int allSize = Media.all().fetch().size();
        renderArgs.put("allSize", allSize);

        int consumedSize = Media.consumed().size();
        renderArgs.put("consumedSize", consumedSize);

        renderArgs.put("period", calcSpentTime(consumedSize));
        render();
    }

    public static void tvshows()
    {
        render();
    }

    public static void movies()
    {
        List<String> movies = Media.find("select m from Media m where m.type = :type")
                .bind("type", MediaType.MOVIE)
                .fetch();

        render(movies);
    }

    public static void books()
    {

        List<String> titles = Media
                .find("select distinct m.title from Media m " +
                        "where m.type = :type " +
                        "order by title")
                .bind("type", MediaType.BOOK)
                .fetch();

        render(titles);
    }

    public static void list(String type)
    {
        List<Media> episodes = new ArrayList<Media>();
        if("recent".equals(type))
        {
            episodes = Media.past(10);
        }
        else if("upcoming".equals(type))
        {
            episodes = Media.upcoming();
        }


        render(episodes, type);
    }

    private static List<String> getTvShows()
    {
        return Media
                    .find("select distinct m.title from Media m " +
                            "where m.type = :type " +
                            "order by title")
                    .bind("type", MediaType.TV_SHOW)
                    .fetch();
    }


    public static void show(Long id)
    {

        Media media = Media.findById(id);
        render(media);
    }

    public static void importFromEpguide(String series)
    {
        //TODO; pack this code where it belongs, a own play module
        System.out.println("starting import for " + series);

        String htmlFile = "/tmp/" + series;
        String curl = "/usr/bin/curl";
        String epguideUrl = "http://epguides.com/" + series + "/";

        String awk = "/usr/bin/awk";
        String ymlfile = "/tmp/" + series + ".yml";

        // TODO: get this from elsewhere, do no hardcode this
        String importScript = "/home/knm/projects/episoder-play/episoder_helper_epguides_yml.awk";
        List<String> curlCommand = Arrays.asList(
                curl, epguideUrl, "--output" , htmlFile);
        List<String> awkCommand = Arrays.asList(
                awk, "-f", importScript, "showid="+series, "output="+ymlfile,
                "salt=" + UUID.randomUUID() ,
                htmlFile);

        String picFile = "/tmp/" + series + ".jpg";
        List<String> pictureCommand = Arrays.asList(
                curl,
                epguideUrl + "cast.jpg",
                "--output" , picFile
        );

        SystemCommandExecutor curlExecutor = new SystemCommandExecutor(curlCommand);
        SystemCommandExecutor awkExecutor = new SystemCommandExecutor(awkCommand);
        SystemCommandExecutor picExecutor = new SystemCommandExecutor(pictureCommand);
        try
        {
            int result = curlExecutor.executeCommand();
            // get the stderr from the command that was run
            StringBuilder stderr = curlExecutor.getStandardErrorFromCommand();

            // print the stdout and stderr
            if(result != 0)
            {
                System.out.println("The numeric result of the curl command was: " + result);
                System.out.println("STDERR:");
                System.out.println(stderr);
            }

            result = awkExecutor.executeCommand();
            stderr = awkExecutor.getStandardErrorFromCommand();

            // print the stdout and stderr
            if(result != 0)
            {
                System.out.println("The numeric result of the awk command was: " + result);
                System.out.println("STDERR:");
                System.out.println(stderr);
            }
            String path = new File(".").getAbsolutePath();
            File targetFile = new File(path + "/conf/" + series + ".yml");
            FileUtils.copyFile(new File(ymlfile),
                    targetFile);
            Fixtures.loadModels(series + ".yml");
            Map<?, ?> map = Fixtures.loadYamlAsMap(series + ".yml");
            Map<?,?> firstmap = (Map<?, ?>) map.values().toArray()[0];

            if(firstmap != null)
            {
                String title = (String) firstmap.get("title");

                // import picture from epguides.com
                TVShow tvShow = new TVShow();
                tvShow.title = title;

                result = picExecutor.executeCommand();
                stderr = picExecutor.getStandardErrorFromCommand();

                // print the stdout and stderr
                if(result != 0)
                {
                    System.out.println(
                            "The numeric result of the picture command was: " + result);
                    System.out.println("STDERR:");
                    System.out.println(stderr);
                }
                File file = new File(picFile);
                if(file.exists() && file.length() > 0)
                {
                    tvShow.picture = new Blob();
                    tvShow.picture.set(new FileInputStream(file),
                            MimeTypes.getContentType(file.getName()));
                    tvShow.save();
                }
            }



            targetFile.delete();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }

        flash.success("%s was imported successfully", series);
        System.out.println("import done for " + series);
        render();
    }

    public static void consume(Long id, boolean consumed)
    {
        if(id != null)
        {
            Media media = Media.findById(id);
            media.consumed = consumed;
            media.save();
        }
        renderJSON(true);
    }

    public static void wish(Long id, boolean wish)
    {
        if(id != null)
        {
            User user = User.find("byEmail", Security.connected()).first();
            Media media = Media.findById(id);
            if(!wish)
            {
                System.out.println("adding");
                user.wishlist.add(media);
                media.user = user;
            }
            else
            {
                System.out.println("removing");
                user.wishlist.remove(media);
                media.user = null;
            }
            media.save();
        }
    }

    public static void episode(String title)
    {
        List<Media> show = new ArrayList<Media>();
        if(title != null)
        {
            show = Media.find(
                "select m from Media m where title = ? order by publishingDate asc",title)
               .fetch();
            boolean consumed = false;
            consumed = Media.isShowConsumed(title);
            String period = calcSpentTime(show.size());
            render(title, show, consumed, period);
        }

        render();
    }

    public static void tvShowPicture(String  title)
    {
        TVShow show = TVShow.find("byTitle", title).first();
        notFoundIfNull(show);
        response.setContentTypeIfNotSet(show.picture.type());
        renderBinary(show.picture.get());
    }

    public static void blank() throws Exception
    {
        CRUD.ObjectType type = CRUD.ObjectType.get(Medias.class);
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Model object = (Model) constructor.newInstance();
        try {
            render(type, object);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object);
        }


        render();

    }

    public static void create() throws Exception
    {
        CRUD.ObjectType type = CRUD.ObjectType.get(Medias.class);
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Model object = (Model) constructor.newInstance();
        Binder.bind(object, "object", params.all());
        validation.valid(object);
        if(validation.hasErrors())
        {
            Map<String,List<Error>> errorMap = validation.errorsMap();
            // TODO: make usefull error messages
//            flash.error(errorMap.values().toString());
            flash.error("Validation failed");
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try
            {
                render(request.controller.replace(".", "/") + "/blank.html",
                        type, object);
            }
            catch(TemplateNotFoundException e)
            {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        flash.success(Messages.get("crud.created", type.modelName));
        if(params.get("_save") != null)
        {
            redirect(request.controller + ".list");
        }
        if(params.get("_saveAndAddAnother") != null)
        {
            redirect(request.controller + ".blank");
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void consumeShow(String title, boolean consume)
    {
        // usually it would get the title directly but jquery delivers the wrong title
        // so i need to do this workaround.
        List<Media> shows = Media.find("byTitle", title).fetch();

        for(Media show : shows)
        {
            show.consumed  = consume;
            show.save();
        }
        episode(title);
    }

    public static void deleteShow(String title)
    {
        List<Media> shows = Media.find("byTitle", title).fetch();
        for(Media show : shows)
        {
            show.delete();
        }
        TVShow show = TVShow.find("byTitle", title).first();
        show.delete();
        index();
    }

    private static String calcSpentTime(int consumedMedias)
    {
        int minutes = consumedMedias * 45;
        Minutes min = Minutes.minutes(minutes);
        Period period = new Period(min);
        period = period.normalizedStandard();
        PeriodFormatter format = new PeriodFormatterBuilder()
            .appendMonths()
            .appendSuffix(" month", " months")
            .appendSeparator(" and ")
            .appendWeeks()
            .appendSuffix(" week", " weeks")
            .appendSeparator(" and ")
            .appendDays()
            .appendSuffix(" day", " days")
            .appendSeparator(" and ")
            .appendMinutes()
            .appendSuffix(" hour", " hours")
            .appendSeparator(" and ")
            .appendHours()
            .appendSuffix(" minute", " minutes")
            .toFormatter();

        String result = format.print(period);
        return result;
    }
}
