package controllers;

import models.User;

/**
 * Handling Auth
 *
 * @author knm
 */
public class Security extends Secure.Security
{
    static boolean authentify(String username, String password)
    {
        return User.connect(username, password) != null;
    }
}
